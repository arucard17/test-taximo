/* eslint-disable array-element-newline */
/* eslint-disable multiline-ternary */
/* eslint-disable no-ternary */
/* eslint-disable require-unicode-regexp */
/* eslint-disable no-process-env */
/* eslint-disable prefer-destructuring */
/* eslint-disable one-var */
/* global __dirname, process */

const HtmlWebpackPlugin = require('html-webpack-plugin'),
    MiniCssExtractPlugin = require("mini-css-extract-plugin"),
    path = require('path'),
    UglifyJsPlugin = require('uglifyjs-webpack-plugin'),
    webpack = require('webpack');

const APP_DIR = path.resolve(__dirname, 'src'),
    BUILD_DIR = path.resolve(__dirname, 'public'),
    isProdBuild = process.env.NODE_ENV === "production";


const config = {
    mode: isProdBuild ? 'production' : 'development',
    entry: `${APP_DIR}/index.js`,
    output: {
        path: BUILD_DIR,
        filename: 'assets/js/bundle.js'
    },
    resolve: {
        extensions: ['.js', '.json'],
        modules: [APP_DIR, 'node_modules']
    },
    devtool: isProdBuild ? "source-map" : "#inline-source-map",
    module: {
        rules: [
            // css
            {
                test: /\.css$/,
                include: /node_modules/,
                loader: [
                    "style-loader",
                    "css-loader"
                ]
            },
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: "css-loader"
                    },
                    {
                        loader: "sass-loader",
                        options: {
                            sassOptions: {
                                includePaths: [path.resolve(__dirname, 'node_modules')]   
                            }
                        }
                    }

                ]
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                include: APP_DIR
            },
            {
                test: /\.(png|gif|jpg|woff|woff2|eot|ttf|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: "file-loader",
                options: {
                    name: "[path][name].[ext]",
                    context: "src"
                }
            },
            {
                test: /\.html$/,
                loader: 'html-loader'
            }
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            CANVAS_RENDERER: true,
            WEBGL_RENDERER: true
        }),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: 'src/index.html'
        }),
        new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output
            // both options are optional
            filename: "[name].css",
            chunkFilename: "[id].css"
        })
    ],
    devServer: {
        contentBase: BUILD_DIR,
        port: 9000,
        stats: 'minimal'
    }
};

if (isProdBuild) {
    config.plugins.optimization = {
        minimizer: [new UglifyJsPlugin()],
    };
}

module.exports = config;
