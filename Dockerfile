FROM node:10.16.3-jessie

COPY . /app
WORKDIR /app

RUN npm install http-server -g

CMD http-server ./public -p 3000