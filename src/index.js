// Styles
import "./assets/styles/app.scss";

// Dependencies
import "jquery";
import "bootstrap/dist/js/bootstrap.bundle.min.js";


/* Init Main Script */
import Main from './assets/js/main.js';

new Main();