import jQuery from 'jquery';
import { formatMoney } from './utils.js';

class CounterUp {

    constructor(item) {
        this.$el = jQuery(item);
        this.value = 0;
        this.old = 0;

        // Animation count config
        this.stepFactor = 0.008;

        // Default element config
        this.config = {
            'step': null, // number step to value adding
            'initial': 0, // initial value
            'delay': 1000, // delay between steps
            'limit': null, // limit number from step config
            'delayBefore': null, // delay before reach the limit for stepBefore config
            'stepBefore': null // step before reach the limit to value adding
        };
        
        this.config = jQuery.extend({}, this.config, this.getConfig()); // Join default config and element config

        // If step is not define, limit is default to animate autoimatic from initial to limit
        if (this.config.step === null) {
            this.config.step = this.config['limit'];
        }

        // Fix countUp scope for timeout function
        this.countUp = this.countUp.bind(this);
    }


    /**
     * getConfig
     * 
     * Method to get config from element data attribute.
     * The accepted attributes is defined in default element config. 
     */
    getConfig() {
        let data = this.$el.data();

        delete data.ride;

        for (let i in data) {
            data[i] = +data[i];

            if (isNaN(data[i])) throw 'Error en la configuración del contador.';
        }

        return data;
    }


    /**
     * run
     * 
     * launch counter
     */
    run() {
        this.value = this.old = this.config.initial;
        this.updateNumber();

        this.countUp();
    }


    /**
     * countUp
     * 
     * Generate next value to render in view.
     */
    countUp() {        
        if(this.config['limit'] === null){
            this.value += this.config.step;
            this.iniTimeOut();
        }

        if (this.config['limit'] !== null && this.value < this.config['limit']){
            if ((this.value + this.config.step) > this.config['limit'])
                this.value = this.config['limit'];
            else
                this.value += this.config.step;
            
            this.iniTimeOut();
        }else if (this.config['limit'] !== null && this.value >= this.config['limit'] && 'stepBefore' in this.config) {
            setTimeout(() => {
                this.value += this.config['stepBefore'];
                this.updateNumber();
                this.countUp();
            }, ('delayBefore' in this.config ? this.config['delayBefore'] : this.config['delay']));
        }
    }


    /**
     * iniTimeOut
     * 
     * Centralize update view number and timeout
     * 
     * @param {number} delay 
     */
    iniTimeOut(delay) {
        if (typeof delay === 'undefined') delay = this.config['delay'];
        this.updateNumber();

        setTimeout(this.countUp, delay);
    }


    /**
     * updateNumber
     * 
     * Generate params to render number in view
     */
    updateNumber() {        
        let delay = 10,
            diff = this.value - this.old,
            step = Math.round(diff * this.stepFactor);

        if (step <= 0) step = 1;

        this.renderNumber(step, delay);
    }

    /**
     * 
     * renderNumber
     * 
     * render number in view
     * 
     * @param {number} step 
     * @param {number} delay 
     */
    renderNumber(step, delay) {
        if (this.old < this.value){
            if ((this.old + step) > this.value)
                this.old = this.value;
            else
                this.old += step;
            
            //  Render number in view
            this.$el[0].innerHTML = '$' + formatMoney(this.old, 0);
            
            setTimeout(() => {
                this.renderNumber(step, delay);
            }, delay);
        } else {
            this.old = this.value;
        }
    }
}

export default CounterUp;
