import jQuery from 'jquery';
import CounterUp from './counterup';

class Main {
    constructor() {
        this.books = [];
        this.counters = [];
        
        this.$modalShowMore = jQuery('#showMoreModal');
        this.$modalShowMoreContent = jQuery('.modal-body', this.$modalShowMore);
        
        this.initCounters();
        
        this.bindEvents();
    }

    initCounters() {
        // Catch all elements with counter-up config and initialize
        jQuery('[data-ride=counter-up]').each((ind, item) => {
            this.counters.push(new CounterUp(item));
        });
        
        // Run counters
        this.counters.forEach(counter => counter.run());
    }

    bindEvents() {
        jQuery('#btnShowMore').on('click', this.showMore.bind(this));
    }


    //////////////////////////////////
    // Show More Modal functionality
    //////////////////////////////////

    /**
     * showMore
     * 
     * Open modal and load data from author
     */
    showMore() {
        this.$modalShowMore.modal('show');

        this.$modalShowMoreContent.empty();

        this.$modalShowMoreContent.append(`<p class="message text-center">Cargando...</p>`);

        if (this.books.length > 0) {
            // Render cached data
            this.renderData(this.books);
        } else {
            // Load data if call the first time
            this.loadData()
                .then(data => {
                    // Cache data
                    this.books = data;

                    this.renderData(data);
                })
                .catch(() => {
                    this.$modalShowMoreContent.find('p.message').text(`Error al cargar la información.`);
                });
        }
    }


    /**
     * loadData
     * 
     * Make request to openlibrary to get books from author
     */
    loadData() {
        return new Promise((resolve, reject) => {
            jQuery.get('http://openlibrary.org/search.json', {
                'author': 'tolkien'
            })
            .done(function (response) {
                response = JSON.parse(response);
                
                resolve(response.docs)
            })
            .fail(function () {
                reject()
            })
        })
    }


    /**
     * renderData
     * 
     * Render author books in modal
     * 
     * @param {Array} data author books
     */
    renderData(data) {
        this.$modalShowMoreContent.empty();

        let $container = jQuery(`<ul></ul>`);

        for (const item of data) {
            $container.append(`<li>${item.title}</li>`);
        }

        this.$modalShowMoreContent.append($container);
    }
}

export default Main;